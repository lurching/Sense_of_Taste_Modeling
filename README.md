# Ajinomatrix Sensory Middleware

Welcome to the Ajinomatrix Sensory Middleware project! Our middleware is a cutting-edge solution tailored for sensory evaluation in the food industry. It integrates data from various sources, including sensors (e-noses, e-tongues, chromatographs), other connected devices, and human psychometric sensory evaluation panels. The middleware processes and stores measurements in versatile JSON files and leverages AI and MongoDB for efficient data handling.

## Project Highlights

- **Proof of Concept**: We have successfully developed a proof of concept that predicts sensory smell impressions based on ingredient data. While we're not sharing this specialized platform just yet, we're actively working on a full-fledged MVP featuring patent-pending technology from the Hebrew University of Jerusalem, with a focus on texture analysis.

- **Sensory OS**: In collaboration with Limen (DE/IT), we're also developing a Sensory Operating System (Sensory OS) for applications in the Metaverse. This is a distinct PoC project aimed at expanding the boundaries of sensory technology.

- **Platform Study**: Our ongoing platform study explores providing an analysis environment for taste and scent measurements, seamless file exchange, and comparative measurements akin to how we handle vision (image files) and hearing (audio files). Many existing applications and frameworks in this domain are proprietary. We're committed to extending these by employing standards and integrating with high-performance computing resources.

## Get Involved

We believe in open collaboration. Contributions and partnerships are highly encouraged. If you're interested in participating or learning more about the project, please don't hesitate to reach out to us at [francois.wayenberg@ajinomatrix.org](mailto:francois.wayenberg@ajinomatrix.org). We're actively organizing project information and welcome your involvement.

## Project Objectives

Our primary goal is to enhance accessibility to large and complex sensory datasets. The platform is designed to harness the potential of the connected web, surpassing the capabilities of conventional search and analytics platforms and proprietary tools in sensory studies. We store data in JSON file format, ensuring compatibility across various programming languages and enabling easy data sharing with other knowledge centers.

## Upcoming: FlavorGraph

Keep an eye out for the upcoming FlavorGraph feature, which will revolutionize how we analyze and understand flavor profiles. This exciting addition will provide advanced tools for flavor analysis and visualization.

## Keywords

JSON, MongoDB, Sensory, Scent, Fragrance, Smell, Flavor, Taste, Measurement, Evaluation, Organoleptic
